import { environment } from './../environments/environment';
import { Injectable } from '@angular/core';

@Injectable()
export class Globals {
  site = environment.site;
  listuser_url = this.site + '/user/list';
  adduser_url = this.site + '/user/add';
  deleteuser_url = this.site + '/user/delete';
  auth_url = this.site + '/oauth/token';
  listroleunderme_url = this.site + '/role/allunderme';
  myrole_url = this.site + '/role/me';
  listlead_url = this.site + '/lead/list';
  listleadsource_url = this.site + '/leadsource/list';
  addlead_url = this.site + '/lead/add';
  addleadsource_url = this.site + '/leadsource/add';
  listclient_url = this.site + '/client/list';
  listbranch_url = this.site + '/branch/list';
  listproject_url = this.site + '/project/list';
  listposition_url = this.site + '/position/list';
  addclient_url = this.site + '/client/add';
  addbranch_url = this.site + '/branch/add';
  addproject_url = this.site + '/project/add';
  addposition_url = this.site + '/position/add';
  deleteclient_url = this.site + '/client/delete';
  deletebranch_url = this.site + '/branch/delete';
  deleteproject_url = this.site + '/project/delete';
  deleteposition_url = this.site + '/position/delete';
  updateclient_url = this.site + '/client/update';
  updatebranch_url = this.site + '/branch/update';
  updateproject_url = this.site + '/project/update';
  updateposition_url = this.site + '/position/update';
  deletelead_url = this.site + '/lead/delete';
  updatelead_url = this.site + '/lead/update';
  listapplication_url = this.site + '/candidate/list';
  leadphone_url = this.site + '/lead/phone';
  savecandidatestatus_url = this.site + '/candidate/save/status';
  savecandidatedetail_url = this.site + '/candidate/save/detail';
  listnationality_url = this.site + '/nationality/all';
  myInterview_url = this.site + '/lead/myInterview';
  clientInterview_url = this.site + '/candidate/clientInterview';
  incomplete_url = this.site + '/candidate/incomplete';
  listflatpackage_url = this.site + '/flatpackage/list';
  addflatpackage_url = this.site + '/flatpackage/add';
  updateflatpackage_url = this.site + '/flatpackage/update';
  flatpackagebyid_url = this.site + '/flatpackage/getbyid';
  listpercentpackage_url = this.site + '/percentpackage/list';
  addpercentpackage_url = this.site + '/percentpackage/add';
  updatepercentpackage_url = this.site + '/percentpackage/update';
  percentpackagebyid_url = this.site + '/percentpackage/getbyid';
  listcontractpackage_url = this.site + '/contractpackage/list';
  addcontractpackage_url = this.site + '/contractpackage/add';
  updatecontractpackage_url = this.site + '/contractpackage/update';
  contractpackagebyid_url = this.site + '/contractpackage/getbyid';
  leadbyid_url = this.site + '/lead/byid';
  candidatebyid_url = this.site + '/candidate/byid';
}
