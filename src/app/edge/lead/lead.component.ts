import { LeadService } from './../../_services/lead.service';
import { LeadTableField } from './../../_models/lead';
import { Component, ViewChild, AfterViewInit, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { Globals } from '../../globals';
import { UserService } from '../../_services/user.service';
import { HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { AddUserDialogComponent } from '../users/users.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddLeadDialogComponent } from './add-lead-dialog/add-lead-dialog.component';
import { SelectionModel } from '@angular/cdk/collections';
import { DeleteLeadDialogComponent } from './delete-lead-dialog/delete-lead-dialog.component';

@Component({
  selector: 'app-lead',
  templateUrl: './lead.component.html',
  styleUrls: ['./lead.component.css']
})
export class LeadComponent implements AfterViewInit, OnInit {
  ELEMENT_DATA: [LeadTableField];
  displayedColumns = ['select'/*, 'id'*/, 'name', 'surname', 'nickname', 'mobile', 'sourcename', 'recruiter'];
  dataSource = new MatTableDataSource(this.ELEMENT_DATA);
  selection = new SelectionModel<LeadTableField>(true, []);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private userService: UserService,
            private dialog: MatDialog,
            private leadService: LeadService,
            private router: Router) { }

  ngOnInit(): void {
    // Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    // Add 'implements OnInit' to the class.
    this.getLeadList();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  openAddLeadDialog() {
    const dialogRef = this.dialog.open(AddLeadDialogComponent, {
      width: '250px',
      data: {name: 'this.name', animal: 'this.animal'}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('add lead dialog closed');
      this.getLeadList();
    });
  }

  getLeadList() {
    this.userService.leadList().subscribe(
      data => {
        const json = JSON.parse(JSON.stringify(data));
        if (json.code === 0) {
          const arr = json.leadlist;
          this.ELEMENT_DATA = arr.map(x => {
            return this.leadService.leadObject2TableField(x);
          });
          this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.selection = new SelectionModel<LeadTableField>(true, []);
        } else {
          console.log('It\'s not a response : ' + JSON.stringify(data));
        }

      }
    );
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  isSelectedAtleaseOne() {
    return this.selection.selected.length > 0;
  }

  isSelectedOnlyOne() {
    return this.selection.selected.length === 1;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  deleteLead() {
    const dialogRef = this.dialog.open(DeleteLeadDialogComponent, {
      width: '250px',
      data: this.selection.selected
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('delete dialog closed');
      this.selection.clear();
      this.getLeadList();
    });
  }

  onRowClick(row) {
    this.router.navigate(['/crm/lead', row.rawData.id]);
  }

}
