import { finalize } from 'rxjs/operators';
import { LeadTableField } from './../../../_models/lead';
import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from '../../../_services/user.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http/src/response';

@Component({
  selector: 'app-delete-lead-dialog',
  templateUrl: './delete-lead-dialog.component.html',
  styleUrls: ['./delete-lead-dialog.component.css']
})
export class DeleteLeadDialogComponent implements OnInit {

  constructor(private userservice: UserService,
    private dialogRef: MatDialogRef<DeleteLeadDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public selection: LeadTableField[],
    private router: Router,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
  }

  onNoClick() {
    this.dialogRef.close();
  }

  onYesClick() {
    console.log(this.selection);
    console.log(this.selection.map(x => x.rawData.id));
    this.userservice.deleteLead(this.selection.map(x => x.rawData.id)).pipe(
      finalize(() => this.dialogRef.close()),
    ).subscribe(
      data => {
        console.log(data);
        if (JSON.parse(JSON.stringify(data)).code === 0) {
          console.log('delete user successfully.');
        } else {
          console.log('unknown reponse : ' + data);
        }
      }
    );
  }
}
