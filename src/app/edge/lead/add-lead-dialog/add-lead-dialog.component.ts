import { finalize } from 'rxjs/operators';
import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from '../../../_services/user.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-lead-dialog',
  templateUrl: './add-lead-dialog.component.html',
  styleUrls: ['./add-lead-dialog.component.css']
})
export class AddLeadDialogComponent implements OnInit {

  constructor(
    private userService: UserService,
    private dialogRef: MatDialogRef<AddLeadDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private formBuilder: FormBuilder) { }

  name: string;
  surname: string;
  nickname: string;
  mobile: string;
  sources: [any] = null;
  currentSourceId: number;

  ngOnInit() {

    this.userService.leadsourceList().subscribe(
      data => {
        console.log(data);
        const json = JSON.parse(JSON.stringify(data));
        if (json.code === 0) {
          this.sources = json.leadsourcelist;
        } else {
          alert(json.message);
        }
        console.log(this.sources);
        if (this.sources.length > 0) {
          this.currentSourceId = this.sources[0].id;
        }
      }
    );
  }

  onCancelClick() {
    this.dialogRef.close();
  }

  onSaveClick() {
    this.userService.createLead(this.name, this.surname, this.nickname, this.mobile, this.currentSourceId).pipe(
      finalize(() => this.dialogRef.close()),
    ).subscribe(
      data => {
        const json = JSON.parse(JSON.stringify(data));
        if (json.code === 0) {
          console.log('create lead ' + this.nickname + ' successfully.');
        } else {
          console.log('It\'s not a response : ' + JSON.stringify(data));
        }
      }
    );
  }
}
