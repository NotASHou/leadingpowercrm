import { SplittedDate } from './../../../_models/candidate';
import { LeadService } from './../../../_services/lead.service';
import { LeadStatus } from './../../../_models/lead';
import { MatDatepicker, MatDatepickerInputEvent } from '@angular/material/datepicker';
import { UserService } from './../../../_services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { LeadByIdResponse } from '../../../_models/lead';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-lead-detail',
  templateUrl: './lead-detail.component.html',
  styleUrls: ['./lead-detail.component.css']
})
export class LeadDetailComponent implements OnInit {

  @ViewChild('picker') picker: MatDatepicker<Date>;

  lead: any;

  title: string;
  name: string;
  surname: string;
  nickname: string;
  mobile: string;
  sources: [any] = null;
  currentSourceId: number;
  currentStatus: LeadStatus;
  date: Date;
  time: string;
  minDate: Date;

  constructor(private user: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private leadService: LeadService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = +params['id']; // (+) converts string 'id' to a number
      this.user.leadById(id).subscribe(data => {
        const r = new LeadByIdResponse(data);
        this.lead = r.lead;
        if (r.code === 0) {
          const today =  new Date();
          const tomorrow =  new Date(today.setDate(today.getDate() + 1));

          this.title = this.lead.nickname;
          this.minDate = tomorrow;
          this.name = this.lead.name;
          this.surname = this.lead.surname;
          this.nickname = this.lead.nickname;
          this.mobile = this.lead.mobile;
          this.currentStatus = this.lead.status;

          if (!this.lead.datetime) {
            this.date = tomorrow;
            this.time = '';
          } else {
            this.date = new Date(this.lead.datetime);
            const datePipe = new DatePipe('en-US');
            this.time = datePipe.transform(this.date, 'HH:mm');
          }

          this.user.leadsourceList().subscribe(
            data => {
              console.log(data);
              const json = JSON.parse(JSON.stringify(data));
              if (json.code === 0) {
                this.sources = json.leadsourcelist;
              } else {
                alert(json.message);
              }
              console.log(this.sources);
              if (this.sources.length > 0) {
                this.currentSourceId = this.sources[0].id;
              }
            }
          );
        } else {
          alert(r.message);
        }
      });
    });
  }

  onCancel() {
    this.router.navigate(['/crm/lead/']);
  }

  back() {
    this.router.navigate(['/crm/lead/']);
  }

  onSaveClick() {
    const splittedDate = new SplittedDate(this.date, this.time);

    this.user.editLead(this.lead.id, this.name, this.surname, this.nickname,
      this.mobile, this.currentSourceId, this.currentStatus, new SplittedDate(this.date, this.time).mergeDate()).subscribe(
      data => {
        const json = JSON.parse(JSON.stringify(data));
        if (json.code === 0) {
          console.log('update lead ' + this.nickname + ' successfully.');
          this.back();
        } else {
          alert(json.message);
        }
      }
    );
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    // console.log(type);
    // console.log(event.value);
    this.date = event.value;
  }

  phone() {
    this.user.leadPhone(this.lead.id).subscribe(
      data => {
        const json = JSON.parse(JSON.stringify(data));
        if (json.code === 0) {
          console.log('convert ' + this.nickname + ' to candidate by phone successfully.');
          this.back();
        } else {
          alert(json.message);
        }
      }
    );
  }

  nextState(current: LeadStatus): string[] {
    switch (current) {
      case LeadStatus.noaction :
        return [LeadStatus.call, LeadStatus.conversation, LeadStatus.interest, LeadStatus.appoint];
      case LeadStatus.call :
        return [LeadStatus.conversation, LeadStatus.interest, LeadStatus.appoint];
      case LeadStatus.conversation :
        return [LeadStatus.interest, LeadStatus.appoint];
      case LeadStatus.interest :
        return [LeadStatus.appoint];
      case LeadStatus.appoint :
        return [LeadStatus.pass];
      case LeadStatus.pass :
        return [];
    }
    return [];
  }

  isShowDate(status: LeadStatus): boolean {
    return status === LeadStatus.appoint;
  }

  isCandidate(lead: any): boolean {
    if (lead.candidate) {
      return true;
    } else {
      return false;
    }
  }

  phoneLabel(lead: any): string {
    if (lead.candidate) {
      return 'Already Candidate';
    } else {
      return 'Phone';
    }
  }
}
