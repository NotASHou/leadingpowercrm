import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientInterviewComponent } from './client-interview.component';

describe('ClientInterviewComponent', () => {
  let component: ClientInterviewComponent;
  let fixture: ComponentFixture<ClientInterviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientInterviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientInterviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
