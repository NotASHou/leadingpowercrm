import { UserService } from './../../_services/user.service';
import { Component, OnInit } from '@angular/core';
import { ClientInterviewDate } from '../../_models/candidate';

@Component({
  selector: 'app-client-interview',
  templateUrl: './client-interview.component.html',
  styleUrls: ['./client-interview.component.css']
})
export class ClientInterviewComponent implements OnInit {

  clientInterviewDateList: ClientInterviewDate[];

  constructor(private user: UserService) { }

  ngOnInit() {
    this.user.clientInterview().subscribe(data => {
      const json = JSON.parse(JSON.stringify(data));
      if (json.code === 0) {
        console.log(json);
        this.clientInterviewDateList = [];
        this.clientInterviewDateList = json.list;

      } else if (json.message) {
        alert(json.message);
      } else {
        console.log('It\'s not a response : ' + JSON.stringify(data));
      }
    });

  }

}
