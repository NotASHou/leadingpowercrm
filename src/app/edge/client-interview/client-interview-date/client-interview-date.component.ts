import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-client-interview-date',
  templateUrl: './client-interview-date.component.html',
  styleUrls: ['./client-interview-date.component.css']
})
export class ClientInterviewDateComponent implements OnInit {

  @Input() interviewDate: any;
  constructor() { }

  ngOnInit() {
  }

}
