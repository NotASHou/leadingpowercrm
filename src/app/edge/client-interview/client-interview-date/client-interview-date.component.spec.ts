import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientInterviewDateComponent } from './client-interview-date.component';

describe('ClientInterviewDateComponent', () => {
  let component: ClientInterviewDateComponent;
  let fixture: ComponentFixture<ClientInterviewDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientInterviewDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientInterviewDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
