import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-c-interview',
  templateUrl: './c-interview.component.html',
  styleUrls: ['./c-interview.component.css']
})
export class CInterviewComponent implements OnInit {

  @Input() candidate: any;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onClick(id) {
    this.router.navigate(['/crm/application', id]);
  }
}
