import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CInterviewComponent } from './c-interview.component';

describe('CInterviewComponent', () => {
  let component: CInterviewComponent;
  let fixture: ComponentFixture<CInterviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CInterviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CInterviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
