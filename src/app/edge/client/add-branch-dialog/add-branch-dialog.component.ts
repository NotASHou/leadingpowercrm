import { finalize } from 'rxjs/operators';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from '../../../_services/user.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-branch-dialog',
  templateUrl: './add-branch-dialog.component.html',
  styleUrls: ['./add-branch-dialog.component.css']
})
export class AddBranchDialogComponent implements OnInit {

  constructor(private userService: UserService,
    private dialogRef: MatDialogRef<AddBranchDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private formBuilder: FormBuilder) { }

  title: string;
  name: string;

  ngOnInit() {
    if (this.data.currentBranch) {
      this.title = this.data.currentBranch.name;
      this.name = this.data.currentBranch.name;
    } else {
      this.title = 'New Branch';
    }

  }

  onCancelClick() {
    this.dialogRef.close();
  }

  onSaveClick() {
    if (this.data.currentBranch) {
      this.userService.updateBranch(this.data.currentBranch.id, this.name).pipe(
        finalize(() => this.dialogRef.close()),
      ).subscribe(
        data => {
          const json = JSON.parse(JSON.stringify(data));
          if (json.code === 0) {
            console.log('update branch ' + this.name + ' successfully.');
          } else {
            console.log('It\'s not a response : ' + JSON.stringify(data));
          }
        }
      );
    } else {
      this.userService.createBranch(this.name, this.data.clientId).pipe(
        finalize(() => this.dialogRef.close()),
      ).subscribe(
        data => {
          const json = JSON.parse(JSON.stringify(data));
          if (json.code === 0) {
            console.log('create branch ' + this.name + ' successfully.');
          } else {
            console.log('It\'s not a response : ' + JSON.stringify(data));
          }
        }
      );
    }

  }

}
