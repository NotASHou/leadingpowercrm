import { finalize } from 'rxjs/operators';
import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from '../../../_services/user.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-project-dialog',
  templateUrl: './add-project-dialog.component.html',
  styleUrls: ['./add-project-dialog.component.css']
})
export class AddProjectDialogComponent implements OnInit {

  constructor(private userService: UserService,
    private dialogRef: MatDialogRef<AddProjectDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private formBuilder: FormBuilder) { }

  title: string;
  name: string;

  ngOnInit() {
    if (this.data.currentProject) {
      this.title = this.data.currentProject.name;
      this.name = this.data.currentProject.name;
    } else {
      this.title = 'New Project';
    }
  }

  onCancelClick() {
    this.dialogRef.close();
  }

  onSaveClick() {
    if (this.data.currentProject) {
      this.userService.updateProject(this.data.currentProject.id, this.name).pipe(
        finalize(() => this.dialogRef.close()),
      ).subscribe(
        data => {
          const json = JSON.parse(JSON.stringify(data));
          if (json.code === 0) {
            console.log('update client ' + this.name + ' successfully.');
          } else {
            console.log('It\'s not a response : ' + JSON.stringify(data));
          }
        }
      );
    } else {
      this.userService.createProject(this.name, this.data.branchId).pipe(
        finalize(() => this.dialogRef.close()),
      ).subscribe(
        data => {
          const json = JSON.parse(JSON.stringify(data));
          if (json.code === 0) {
            console.log('create client ' + this.name + ' successfully.');
          } else {
            console.log('It\'s not a response : ' + JSON.stringify(data));
          }
        }
      );
    }

  }

}
