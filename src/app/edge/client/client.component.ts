import { Component, ViewChild, OnInit } from '@angular/core';
import { MatSelectionList, MatListOption, MatSelectionListChange } from '@angular/material/list';
import { SelectionModel } from '@angular/cdk/collections';
import { UserService } from '../../_services/user.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { AddClientDialogComponent } from './add-client-dialog/add-client-dialog.component';
import { AddBranchDialogComponent } from './add-branch-dialog/add-branch-dialog.component';
import { AddProjectDialogComponent } from './add-project-dialog/add-project-dialog.component';
import { AddPositionDialogComponent } from './add-position-dialog/add-position-dialog.component';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  @ViewChild('clients') clients: MatSelectionList;
  @ViewChild('branches') branches: MatSelectionList;
  @ViewChild('projects') projects: MatSelectionList;
  @ViewChild('positions') positions: MatSelectionList;

  clientList: any[];
  branchList: any[];
  projectList: any[];
  positionList: any[];

  currentClientIndex: number;
  currentBranchIndex: number;
  currentProjectIndex: number;
  currentPositionIndex: number;

  constructor(private userService: UserService,
    private router: Router,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.getClientList();
  }

  onClientSelection(e, v) {
    const tmp = e.option.selected;
    this.clients.deselectAll();
    e.option.selected = tmp;

    if (v.length > 0) {
      this.currentClientIndex = this.clientList.map(x => x.id).indexOf(v[v.length - 1].value.id);
      this.branchList = v[v.length - 1].value.branches;
    } else {
      this.currentClientIndex = null;
      this.branchList = [];
    }
    this.projectList = [];
    this.positionList = [];
  }

  onBranchSelection(e, v) {
    const tmp = e.option.selected;
    this.branches.deselectAll();
    e.option.selected = tmp;

    if (v.length > 0) {
      this.currentBranchIndex = this.branchList.map(x => x.id).indexOf(v[v.length - 1].value.id);
      this.projectList = v[v.length - 1].value.projects;
    } else {
      this.currentBranchIndex = null;
      this.projectList = [];
    }
    this.positionList = [];
  }

  onProjectSelection(e, v) {
    const tmp = e.option.selected;
    this.projects.deselectAll();
    e.option.selected = tmp;

    if (v.length > 0) {
      this.currentProjectIndex = this.projectList.map(x => x.id).indexOf(v[v.length - 1].value.id);
      this.positionList = v[v.length - 1].value.positions;
    } else {
      this.currentProjectIndex = null;
      this.positionList = [];
    }
  }

  onPositionSelection(e, v) {
    const tmp = e.option.selected;
    this.positions.deselectAll();
    e.option.selected = tmp;

    if (v.length > 0) {
      this.currentPositionIndex = this.positionList.map(x => x.id).indexOf(v[v.length - 1].value.id);
    } else {
      this.currentPositionIndex = null;
    }
  }

  getClientList() {
    this.userService.clientList().subscribe(
      data => {
        const json = JSON.parse(JSON.stringify(data));
        if (json.code === 0) {
          this.clientList = json.clients;
          this.branchList = [];
          this.projectList = [];
          this.positionList = [];
        } else {
          console.log('It\'s not a response : ' + JSON.stringify(data));
        }

      }
    );
  }

  getBranchList(clientId: number) {
    this.userService.branchList(clientId).subscribe(
      data => {
        const json = JSON.parse(JSON.stringify(data));
        console.log(json);
        if (json.code === 0) {
          this.clientList[this.currentClientIndex].branches = json.branches;
          this.branchList = json.branches;
          this.projectList = [];
          this.positionList = [];
        } else {
          console.log('It\'s not a response : ' + JSON.stringify(data));
        }

      }
    );
  }

  getProjectList(branchId: number) {
    this.userService.projectList(branchId).subscribe(
      data => {
        const json = JSON.parse(JSON.stringify(data));
        if (json.code === 0) {
          this.clientList[this.currentClientIndex].branches[this.currentBranchIndex].projects = json.projects;
          this.branchList[this.currentBranchIndex].projects = json.projects;
          this.projectList = json.projects;
          this.positionList = [];
        } else {
          console.log('It\'s not a response : ' + JSON.stringify(data));
        }

      }
    );
  }

  getPositionList(projectId: number) {
    this.userService.positionList(projectId).subscribe(
      data => {
        const json = JSON.parse(JSON.stringify(data));
        if (json.code === 0) {
          this.clientList[this.currentClientIndex]
          .branches[this.currentBranchIndex]
          .projects[this.currentProjectIndex]
          .positions = json.positions;
          this.branchList[this.currentBranchIndex].projects[this.currentProjectIndex].positions = json.positions;
          this.projectList[this.currentProjectIndex].positions = json.positions;
          this.positionList = json.positions;
        } else {
          console.log('It\'s not a response : ' + JSON.stringify(data));
        }

      }
    );
  }

  addClient() {
    const dialogRef = this.dialog.open(AddClientDialogComponent, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('add client dialog closed');
      this.getClientList();
    });

  }

  addBranch() {
    if (this.currentClientIndex == null) {return; }

    const clientId = this.clientList[this.currentClientIndex].id;
    const dialogRef = this.dialog.open(AddBranchDialogComponent, {
      width: '250px',
      data: {clientId: clientId}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('add branch dialog closed');
      this.getBranchList(clientId);
    });

  }

  addProject() {
    if (this.currentBranchIndex == null) {return; }

    console.log(this.clientList[this.currentClientIndex].branches[this.currentBranchIndex].id);
    const branchId = this.clientList[this.currentClientIndex].branches[this.currentBranchIndex].id;
    const dialogRef = this.dialog.open(AddProjectDialogComponent, {
      width: '250px',
      data: {branchId: branchId}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('add project dialog closed');
      this.getProjectList(branchId);
    });

  }

  addPosition() {
    if (this.currentProjectIndex == null) {return; }

    const projectId = this.clientList[this.currentClientIndex].branches[this.currentBranchIndex].projects[this.currentProjectIndex].id;
    const dialogRef = this.dialog.open(AddPositionDialogComponent, {
      width: '250px',
      data: {projectId: projectId}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('add position dialog closed');
      this.getPositionList(projectId);
    });

  }

  deleteClient() {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: {type: 'client', object: this.clientList[this.currentClientIndex]}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getClientList();
    });

  }

  deleteBranch() {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: {type: 'branch', object: this.branchList[this.currentBranchIndex]}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getBranchList(this.clientList[this.currentClientIndex].id);
    });

  }

  deleteProject() {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: {type: 'project', object: this.projectList[this.currentProjectIndex]}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getProjectList(this.branchList[this.currentBranchIndex].id);
    });

  }

  deletePosition() {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: {type: 'position', object: this.positionList[this.currentPositionIndex]}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getPositionList(this.projectList[this.currentProjectIndex].id);
    });

  }

  updateClient() {
    const dialogRef = this.dialog.open(AddClientDialogComponent, {
      width: '250px',
      data: {currentClient: this.clientList[this.currentClientIndex]}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('update client dialog closed');
      this.getClientList();
    });
  }

  updateBranch() {
    if (this.currentClientIndex == null) {return; }

    const clientId = this.clientList[this.currentClientIndex].id;
    const dialogRef = this.dialog.open(AddBranchDialogComponent, {
      width: '250px',
      data: {currentBranch: this.clientList[this.currentClientIndex].branches[this.currentBranchIndex]}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('update branch dialog closed');
      this.getBranchList(clientId);
    });

  }

  updateProject() {
    if (this.currentBranchIndex == null) {return; }

    const branchId = this.clientList[this.currentClientIndex].branches[this.currentBranchIndex].id;
    const dialogRef = this.dialog.open(AddProjectDialogComponent, {
      width: '250px',
      data: {currentProject: this.clientList[this.currentClientIndex].branches[this.currentBranchIndex].projects[this.currentProjectIndex]}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('update project dialog closed');
      this.getProjectList(branchId);
    });

  }

  updatePosition() {
    if (this.currentProjectIndex == null) {return; }

    const projectId = this.clientList[this.currentClientIndex].branches[this.currentBranchIndex].projects[this.currentProjectIndex].id;
    const dialogRef = this.dialog.open(AddPositionDialogComponent, {
      width: '250px',
      data: {currentPosition: this.clientList[this.currentClientIndex]
                                .branches[this.currentBranchIndex]
                                .projects[this.currentProjectIndex]
                                .positions[this.currentPositionIndex]}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('update position dialog closed');
      this.getPositionList(projectId);
    });

  }

}
