import { finalize } from 'rxjs/operators';
import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from '../../../_services/user.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-position-dialog',
  templateUrl: './add-position-dialog.component.html',
  styleUrls: ['./add-position-dialog.component.css']
})
export class AddPositionDialogComponent implements OnInit {

  constructor(private userService: UserService,
    private dialogRef: MatDialogRef<AddPositionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private formBuilder: FormBuilder) { }

  title: string;
  name: string;

  ngOnInit() {
    if (this.data.currentPosition) {
      this.title = this.data.currentPosition.name;
      this.name = this.data.currentPosition.name;
    } else {
      this.title = 'New Position';
    }
  }

  onCancelClick() {
    this.dialogRef.close();
  }

  onSaveClick() {
    if (this.data.currentPosition) {
      this.userService.updatePosition(this.data.currentPosition.id, this.name).pipe(
        finalize(() => this.dialogRef.close()),
      ).subscribe(
        data => {
          const json = JSON.parse(JSON.stringify(data));
          if (json.code === 0) {
            console.log('update position ' + this.name + ' successfully.');
          } else {
            console.log('It\'s not a response : ' + JSON.stringify(data));
          }
        }
      );
    } else {
      this.userService.createPosition(this.name, this.data.projectId).pipe(
        finalize(() => this.dialogRef.close()),
      ).subscribe(
        data => {
          const json = JSON.parse(JSON.stringify(data));
          if (json.code === 0) {
            console.log('create position ' + this.name + ' successfully.');
          } else {
            console.log('It\'s not a response : ' + JSON.stringify(data));
          }
        }
      );
    }

  }

}
