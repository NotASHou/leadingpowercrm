import { finalize } from 'rxjs/operators';
import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from '../../../_services/user.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.css']
})
export class DeleteDialogComponent implements OnInit {

  constructor(private userService: UserService,
    private dialogRef: MatDialogRef<DeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router) { }

  ngOnInit() {
  }

  onCancelClick() {
    this.dialogRef.close();
  }

  onDeleteClick() {
    if (this.data.type === 'client') {
      this.userService.deleteClient(this.data.object.id).pipe(
        finalize(() => this.dialogRef.close()),
      ).subscribe(
        data => {
          const json = JSON.parse(JSON.stringify(data));
          if (json.code === 0) {
            console.log('delete client ' + this.data.object.name + ' successfully.');
          } else {
            console.log('It\'s not a response : ' + JSON.stringify(data));
          }
        }
      );
    } else if (this.data.type === 'branch') {
      this.userService.deleteBranch(this.data.object.id).pipe(
        finalize(() => this.dialogRef.close()),
      ).subscribe(
        data => {
          const json = JSON.parse(JSON.stringify(data));
          if (json.code === 0) {
            console.log('delete branch ' + this.data.object.name + ' successfully.');
          } else {
            console.log('It\'s not a response : ' + JSON.stringify(data));
          }
        }
      );
    } else if (this.data.type === 'project') {
      this.userService.deleteProject(this.data.object.id).pipe(
        finalize(() => this.dialogRef.close()),
      ).subscribe(
        data => {
          const json = JSON.parse(JSON.stringify(data));
          if (json.code === 0) {
            console.log('delete project ' + this.data.object.name + ' successfully.');
          } else {
            console.log('It\'s not a response : ' + JSON.stringify(data));
          }
        }
      );
    } else if (this.data.type === 'position') {
      this.userService.deletePosition(this.data.object.id).pipe(
        finalize(() => this.dialogRef.close()),
      ).subscribe(
        data => {
          const json = JSON.parse(JSON.stringify(data));
          if (json.code === 0) {
            console.log('delete position ' + this.data.object.name + ' successfully.');
          } else {
            console.log('It\'s not a response : ' + JSON.stringify(data));
          }
        }
      );
    }
  }

}
