import { finalize } from 'rxjs/operators';
import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from '../../../_services/user.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-client-dialog',
  templateUrl: './add-client-dialog.component.html',
  styleUrls: ['./add-client-dialog.component.css']
})
export class AddClientDialogComponent implements OnInit {

  constructor(private userService: UserService,
    private dialogRef: MatDialogRef<AddClientDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private formBuilder: FormBuilder) { }

  title: string;
  name: string;

  ngOnInit() {
    if (this.data.currentClient) {
      this.title = this.data.currentClient.name;
      this.name = this.data.currentClient.name;
    } else {
      this.title = 'New Client';
    }
  }

  onCancelClick() {
    this.dialogRef.close();
  }

  onSaveClick() {
    if (this.data.currentClient) {
      this.userService.updateClient(this.data.currentClient.id, this.name).pipe(
        finalize(() => this.dialogRef.close()),
      ).subscribe(
        data => {
          const json = JSON.parse(JSON.stringify(data));
          if (json.code === 0) {
            console.log('update client ' + this.name + ' successfully.');
          } else {
            console.log('It\'s not a response : ' + JSON.stringify(data));
          }
        }
      );
    } else {
      this.userService.createClient(this.name).pipe(
        finalize(() => this.dialogRef.close()),
      ).subscribe(
        data => {
          const json = JSON.parse(JSON.stringify(data));
          if (json.code === 0) {
            console.log('create client ' + this.name + ' successfully.');
          } else {
            console.log('It\'s not a response : ' + JSON.stringify(data));
          }
        }
      );
    }

  }

}
