import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterviewDateComponent } from './interview-date.component';

describe('InterviewDateComponent', () => {
  let component: InterviewDateComponent;
  let fixture: ComponentFixture<InterviewDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterviewDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterviewDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
