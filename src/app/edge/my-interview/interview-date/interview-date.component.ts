import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-interview-date',
  templateUrl: './interview-date.component.html',
  styleUrls: ['./interview-date.component.css']
})
export class InterviewDateComponent implements OnInit {
  @Input() interviewDate: any;

  date: Date;

  constructor(private router: Router) { }

  ngOnInit() {
    this.date = new Date();
  }

  onClickLead(lead) {
    this.router.navigate(['/crm/lead', lead.id]);
  }
}
