import { Router } from '@angular/router';
import { InterviewDate } from './../../_models/lead';
import { UserService } from './../../_services/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-interview',
  templateUrl: './my-interview.component.html',
  styleUrls: ['./my-interview.component.css']
})
export class MyInterviewComponent implements OnInit {

  interviewDateList: InterviewDate[];

  constructor(private user: UserService,
    private router: Router) { }

  ngOnInit() {
    this.user.myInterview().subscribe(data => {
      const json = JSON.parse(JSON.stringify(data));
      console.log(json);
      if (json.code === 0) {
        this.interviewDateList = [];
        this.interviewDateList = json.list;

      } else if (json.message) {
        alert(json.message);
      } else {
        console.log('It\'s not a response : ' + JSON.stringify(data));
      }
    });
  }
}
