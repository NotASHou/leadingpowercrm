import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_services/authentication.service';
import { Router } from '@angular/router';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-edge',
  templateUrl: './edge.component.html',
  styleUrls: ['./edge.component.css']
})
export class EdgeComponent implements OnInit {

  username: string;
  isAdmin: boolean;

  constructor(private authenticationService: AuthenticationService,
            private userService: UserService,
            private router: Router) { }

  ngOnInit() {
    this.username = this.authenticationService.readUsername();
    this.userService.myRole().subscribe(
      data => {
        const json: any = data;
        if (json.role === 'admin' || json.role === 'superadmin') {
          this.isAdmin = true;
        } else {
          this.isAdmin = false;
        }
      }
    );
  }

  logout() {
    this.router.navigate(['/login']);
  }
}
