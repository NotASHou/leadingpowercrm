import { Router, ActivatedRoute } from '@angular/router';
import { CandidateService } from './../../_services/candidate.service';
import { CandidateTableField } from './../../_models/candidate';
import { UserService } from './../../_services/user.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.css']
})
export class ApplicationComponent implements OnInit {
  ELEMENT_DATA: [CandidateTableField];
  displayedColumns = ['select'/*, 'id'*/, 'name', 'surname', 'status', 'mobile', 'registerDate'];
  dataSource = new MatTableDataSource(this.ELEMENT_DATA);
  selection = new SelectionModel<CandidateTableField>(true, []);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private userService: UserService,
    private dialog: MatDialog,
    private candidateService: CandidateService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.getCandidateList();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  isSelectedOnlyOne() {
    return this.selection.selected.length === 1;
  }

  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  onClickRow(row) {
    this.router.navigate(['./' + row.rawData.id], {relativeTo: this.route});
  }

  getCandidateList() {
    this.userService.applicationList().subscribe(
      data => {
        const json = JSON.parse(JSON.stringify(data));
        console.log(json);
        if (json.code === 0) {
          const arr = json.candidateList;
          this.ELEMENT_DATA = arr.map(x => {
            return this.candidateService.candidateObject2TableField(x);
          });
          // console.log(this.ELEMENT_DATA);
          this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.selection = new SelectionModel<CandidateTableField>(true, []);
        } else {
          console.log('It\'s not a response : ' + JSON.stringify(data));
        }
      }
    );
  }
}
