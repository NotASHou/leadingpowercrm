import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { CandidateStatusData } from './../../../../_models/candidate';
import { UserService } from './../../../../_services/user.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, Input, Output, ViewChild, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { CandidateStatus, SplittedDate } from '../../../../_models/candidate';
import { CandidateService } from '../../../../_services/candidate.service';
import * as _ from 'lodash';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-candidate-status',
  templateUrl: './candidate-status.component.html',
  styleUrls: ['./candidate-status.component.css']
})
export class CandidateStatusComponent implements OnInit {

  @Input() candidate: any;
  data: any;

  prevData: CandidateStatusData;

  clientList: any[];
  branchList: any[];
  projectList: any[];
  positionList: any[];

  constructor(private user: UserService,
    private candidateService: CandidateService,
    private cd: ChangeDetectorRef,
    private router: Router) { }

  ngOnInit() {
    const split =  (this.candidate.date) ?
                  this.candidateService.splitDate(new Date(this.candidate.date)) :
                  {date: null, time: null};
    this.data = { date: split.date,
      time: split.time,
      clientId: this.candidate.client ? this.candidate.client.id : null,
      branchId: this.candidate.branch ? this.candidate.branch.id : null,
      projectId: this.candidate.project ? this.candidate.project.id : null,
      positionId: this.candidate.position ? this.candidate.position.id : null,
      note: this.candidate.note,
      salary: this.candidate.salary,
      status: this.candidate.status};

    console.log(this.data);
    this.getClientList();

    this.prevData = this.candidateService.candidateStatusDataFromForm(this);
  }

  getClientList() {
    this.cd.detach();
    this.user.clientList().pipe(
      finalize(() => this.cd.reattach()),
    ).subscribe(
      data => {
        const json = JSON.parse(JSON.stringify(data));
        if (json.code === 0) {
          this.clientList = json.clients;

          if (this.clientList === []) {
            this.branchList = this.clientList.filter(x => {
              return x.id === this.data.clientId;
            })[0].branches;
          } else {
            this.branchList = [];
          }

          if (this.branchList === []) {
            this.projectList = this.branchList.filter(x => {
              return x.id === this.data.branchId;
            })[0].projects;
          } else {
            this.projectList = [];
          }

          if (this.projectList === []) {
            this.positionList = this.projectList.filter(x => {
              return x.id === this.data.projectId;
            })[0].positions;
          } else {
            this.positionList = [];
          }

        } else {
          console.log('It\'s not a response : ' + JSON.stringify(data));
        }
      }
    );
  }

  getBranchList(clientId): any[] {
    if (clientId == null) { return []; }
    const matched = this.clientList.filter(x => {
      return x.id === clientId;
    });
    if (matched.length > 0) {
      this.branchList = matched[0].branches;
      return this.branchList;
    } else {
      return [];
    }
  }

  getProjectList(branchId): any[] {
    if (branchId == null) { return []; }
    const matched = this.branchList.filter(x => {
      return x.id === branchId;
    });
    if (matched.length > 0) {
      this.projectList = matched[0].projects;
      return this.projectList;
    } else {
      return [];
    }
  }

  getPositionList(projectId): any[] {
    if (projectId == null) { return []; }
    const matched = this.projectList.filter(x => {
      return x.id === projectId;
    });
    if (matched.length > 0) {
      this.positionList = matched[0].positions;
      return this.positionList;
    } else {
      return [];
    }
  }

  nextStage(status: CandidateStatus): CandidateStatus[] {
    switch (status) {
      case CandidateStatus.idle :
        return [CandidateStatus.clientinterview];
      case CandidateStatus.pass :
        return [CandidateStatus.clientinterview];
      case CandidateStatus.clientinterview :
        return [CandidateStatus.firstday, CandidateStatus.trainingcomplete, CandidateStatus.reject];
      case CandidateStatus.firstday :
        return [CandidateStatus.trainingcomplete, CandidateStatus.noshow, CandidateStatus.dropout];
      case CandidateStatus.trainingcomplete :
        return [CandidateStatus.garanteeneed];
      case CandidateStatus.garanteeneed :
        return [];
      case CandidateStatus.reject :
        return [CandidateStatus.idle];
      case CandidateStatus.noshow :
        return [CandidateStatus.idle];
      case CandidateStatus.dropout :
        return [CandidateStatus.idle];
      case CandidateStatus.terminate :
        return [];
    }
    return [];
  }

  candidateStatusLabel(status: CandidateStatus) {
    switch (status) {
      case CandidateStatus.idle :
        return 'Idle';
      case CandidateStatus.pass :
        return 'Pass';
      case CandidateStatus.clientinterview :
        return 'Int';
      case CandidateStatus.firstday :
        return '1st';
      case CandidateStatus.trainingcomplete :
        return 'Cmpt';
      case CandidateStatus.garanteeneed :
        return 'Grnt';
      case CandidateStatus.reject :
        return 'Reject';
      case CandidateStatus.noshow :
        return 'No show';
      case CandidateStatus.dropout :
        return 'Drop out';
      case CandidateStatus.terminate :
        return 'Terminate';
    }

    return '';
  }

  onSave() {
    this.user.saveCandidateStatus(this.candidate.id, this.data.status,
                                  new SplittedDate(this.data.date, this.data.time).mergeDate(),
                                  this.data.clientId,
                                  this.data.branchId,
                                  this.data.projectId,
                                  this.data.positionId,
                                  this.data.note,
                                  this.data.salary).subscribe(
      data => {
        const json = JSON.parse(JSON.stringify(data));
        if (json.code === 0) {
          this.candidate = json.candidate;
          this.prevData = this.candidateService.candidateStatusDataFromForm(this);
        } else {
          console.log('It\'s not a response : ' + JSON.stringify(data));
        }
      }
    );

  }

  onDataChange(data): boolean {
    const currData = this.candidateService.candidateStatusDataFromForm(this);

    // console.log(this.prevData);
    // console.log(currData);
    return !_.isEqual(this.prevData, currData);
  }

  onBack() {
    this.router.navigate(['/crm/application']);
  }

  isShowDateTime(_status: CandidateStatus) {
    const r = [CandidateStatus.clientinterview,
      CandidateStatus.firstday].includes(_status);
    return r;
  }
  isShowClient(_status: CandidateStatus) {
    const r = [CandidateStatus.clientinterview,
      CandidateStatus.firstday,
      CandidateStatus.trainingcomplete].includes(_status);
    return r;
  }
  allowEditClient(_status: CandidateStatus) {
    const r = [CandidateStatus.clientinterview].includes(_status);
    return r;
  }
  isShowSalary(_status: CandidateStatus) {
    const r = [CandidateStatus.clientinterview,
      CandidateStatus.firstday,
      CandidateStatus.trainingcomplete].includes(_status);
    return r;
  }
  isRequireSalary(_status: CandidateStatus) {
    const r = [CandidateStatus.firstday,
      CandidateStatus.trainingcomplete].includes(_status);
    return r;
  }
}
