import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from './../../../_services/user.service';
import { BehaviorSubject } from 'rxjs';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-edit-application',
  templateUrl: './edit-application.component.html',
  styleUrls: ['./edit-application.component.css']
})
export class EditApplicationComponent implements OnInit {
  candidate: any;

  constructor(private user: UserService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = +params['id']; // (+) converts string 'id' to a number
      this.user.candidateById(id).subscribe(data => {
        const json = JSON.parse(JSON.stringify(data));
        if (json.code === 0) {
          this.candidate = json.candidate;
        } else if (json.message) {
          alert(json.message);
        } else {
          console.log('It\'s not a response : ' + JSON.stringify(data));
        }
      });
    });
  }
}
