import { Router } from '@angular/router';
import { CandidateService } from './../../../../_services/candidate.service';
import { CandidateTableField, CandidateDetailData, Nationality } from './../../../../_models/candidate';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import * as _ from 'lodash';
import { UserService } from '../../../../_services/user.service';

@Component({
  selector: 'app-candidate-detail',
  templateUrl: './candidate-detail.component.html',
  styleUrls: ['./candidate-detail.component.css']
})
export class CandidateDetailComponent implements OnInit {

  @Input() candidate: any;
  data: any;

  nationalityList: Nationality[];

  prevData: CandidateDetailData;
  today = new Date();

  constructor(private candidateService: CandidateService,
    private user: UserService,
    private router: Router) { }

  ngOnInit() {
    console.log(this.candidate);
    this.data = {
      name: this.candidate.lead.name,
      surname: this.candidate.lead.surname,
      nickname: this.candidate.lead.nickname,
      mobile: this.candidate.lead.mobile,
      dateOfBirth: this.candidate.dateOfBirth ? new Date(this.candidate.dateOfBirth) : null,
      nationalityId: this.candidate.nationality ? this.candidate.nationality.id : null
    };

    this.prevData = this.candidateService.candidateDetailDataFromForm(this);

    this.user.nationalityList().subscribe( data => {
      const json = JSON.parse(JSON.stringify(data));
      if (json.code === 0) {
        this.nationalityList = json.nationalities.map(x => new Nationality(x.id, x.name));
      } else {
        console.log('It\'s not a response : ' + JSON.stringify(data));
      }
    });

  }

  onSave() {
    this.user.saveCandidateDetail(this.candidate.id, this.data.name, this.data.surname, this.data.nickname, this.data.mobile,
      this.data.nationalityId, this.data.dateOfBirth).subscribe( data => {
        const json = JSON.parse(JSON.stringify(data));
        if (json.code === 0) {
          this.candidate = json.candidate;
          this.prevData = this.candidateService.candidateDetailDataFromForm(this);
        } else {
          console.log('It\'s not a response : ' + JSON.stringify(data));
        }
      });

  }

  onDataChange(data): boolean {
    const currData = this.candidateService.candidateDetailDataFromForm(this);

    return !_.isEqual(this.prevData, currData);
  }

  onBack() {
    this.router.navigate(['/crm/application']);
  }
}
