import { Component, ViewChild, AfterViewInit, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog } from '@angular/material';
import { UserService } from '../../_services/user.service';
import { Router } from '@angular/router';
import { AddLeadSourceDialogComponent } from './add-lead-source-dialog/add-lead-source-dialog.component';

@Component({
  selector: 'app-lead-source',
  templateUrl: './lead-source.component.html',
  styleUrls: ['./lead-source.component.css']
})
export class LeadSourceComponent implements AfterViewInit, OnInit {
  ELEMENT_DATA: [any];
  displayedColumns = ['name', 'cost', 'createdate'];
  dataSource = new MatTableDataSource(this.ELEMENT_DATA);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private userService: UserService,
    private router: Router,
    private dialog: MatDialog) { }

  ngOnInit(): void {
    // Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    // Add 'implements OnInit' to the class.
    this.getLeadSourceList();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  openAddLeadSourceDialog() {
    const dialogRef = this.dialog.open(AddLeadSourceDialogComponent, {
      width: '250px',
      data: {name: 'this.name', animal: 'this.animal'}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('add lead dialog closed');
      this.getLeadSourceList();
    });
  }

  getLeadSourceList() {
    this.userService.leadsourceList().subscribe(
      data => {
        const json = JSON.parse(JSON.stringify(data));
        if (json.code === 0) {
          const arr = json.leadsourcelist;
          this.ELEMENT_DATA = arr;
          this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        } else {
          console.log('It\'s not a response : ' + JSON.stringify(data));
        }

      }
    );
  }
}
