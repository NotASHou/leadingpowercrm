import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLeadSourceDialogComponent } from './add-lead-source-dialog.component';

describe('AddLeadSourceDialogComponent', () => {
  let component: AddLeadSourceDialogComponent;
  let fixture: ComponentFixture<AddLeadSourceDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLeadSourceDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLeadSourceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
