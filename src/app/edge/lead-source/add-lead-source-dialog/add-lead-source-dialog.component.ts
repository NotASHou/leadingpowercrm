import { finalize } from 'rxjs/operators';
import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from '../../../_services/user.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-lead-source-dialog',
  templateUrl: './add-lead-source-dialog.component.html',
  styleUrls: ['./add-lead-source-dialog.component.css']
})
export class AddLeadSourceDialogComponent implements OnInit {

  constructor(
    private userService: UserService,
    private dialogRef: MatDialogRef<AddLeadSourceDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private formBuilder: FormBuilder) { }

  name: string;
  cost: number;

  ngOnInit() {

  }

  onCancelClick() {
    this.dialogRef.close();
  }

  onSaveClick() {
    this.userService.createLeadSource(this.name, this.cost).pipe(
      finalize(() => this.dialogRef.close()),
    ).subscribe(
      data => {
        const json = JSON.parse(JSON.stringify(data));
        if (json.code === 0) {
          console.log('create lead ' + this.name + ' successfully.');
        } else {
          console.log('It\'s not a response : ' + JSON.stringify(data));
        }
      }
    );
  }

}
