import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { MatSort, MatTableDataSource } from '@angular/material';
import { CandidateStatus } from '../../_models/candidate';

@Component({
  selector: 'app-incomplete',
  templateUrl: './incomplete.component.html',
  styleUrls: ['./incomplete.component.css']
})
export class IncompleteComponent implements OnInit {

  candidateList: IncompleteTableField[];

  displayedColumns: string[] = ['name', 'mobile', 'status'];
  dataSource = new MatTableDataSource(this.candidateList);

  @ViewChild(MatSort) sort: MatSort;



  constructor(private user: UserService,
    private router: Router) { }

  ngOnInit() {
    this.dataSource.sort = this.sort;

    this.user.incomplete().subscribe(
      data => {
        const json = JSON.parse(JSON.stringify(data));
        console.log(json);
        if (json.code === 0) {
          this.candidateList = json.list.map(x => {
            const tmp = new IncompleteTableField();
            tmp.rawData = x;
            tmp.name = x.lead.name + ' ' + x.lead.surname;
            tmp.mobile = x.lead.mobile;
            tmp.status = x.status;
            return tmp;
          });
          console.log(this.candidateList);
          this.dataSource = new MatTableDataSource(this.candidateList);
          this.dataSource.sort = this.sort;
        } else if (json.message) {
          alert(json.message);
        } else {
          console.log('It\'s not a response : ' + JSON.stringify(data));
        }
    });
  }

  onClickRow(row) {
    this.router.navigate(['/crm/application', row.rawData.id]);
    // console.log(row.rawData.id);
  }
}

export class IncompleteTableField {
  name: string;
  mobile: string;
  status: CandidateStatus;
  rawData: any;
}
