import { finalize } from 'rxjs/operators';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from '../../../_services/user.service';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Users } from '../users.component';
import { HttpErrorResponse } from '@angular/common/http/src/response';

@Component({
  selector: 'app-delete-user-dialog',
  templateUrl: './delete-user-dialog.component.html',
  styleUrls: ['./delete-user-dialog.component.css']
})
export class DeleteUserDialogComponent implements OnInit {

  constructor(
    private userservice: UserService,
    private dialogRef: MatDialogRef<DeleteUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public selection: Users[],
    private router: Router,
    private formBuilder: FormBuilder) { }


  ngOnInit() {
  }

  onNoClick() {
    this.dialogRef.close();
  }

  onYesClick() {
    this.userservice.deleteUser(this.selection.map(x => x.id)).pipe(
      finalize(() => this.dialogRef.close()),
    ).subscribe(
      data => {
        console.log(data);
        if (JSON.parse(JSON.stringify(data)).code === 0) {
          console.log('delete user successfully.');
        } else {
          console.log('unknown reponse : ' + data);
        }
      },
    );
  }
}
