
import {of as observableOf,  Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http/src/response';
import { Component, ViewChild, AfterViewInit, Inject, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { Http, Response, RequestOptions, Headers, Request, RequestMethod } from '@angular/http';
import { HttpClient, HttpRequest, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Globals } from '../../globals';
import { DataSource } from '@angular/cdk/collections';
import { Data, Router } from '@angular/router';


import { AuthenticationService } from '../../_services/authentication.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from '../../_services/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { DeleteUserDialogComponent } from './delete-user-dialog/delete-user-dialog.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements AfterViewInit {

  ELEMENT_DATA: Array<Users>;
  displayedColumns = ['select', 'username', 'fullname', 'roles', 'reserved'/*, 'active'*/];
  dataSource = new MatTableDataSource(this.ELEMENT_DATA);
  selection = new SelectionModel<Users>(true, []);

  color = 'primary';
  checked = true;
  disabled = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dialog: MatDialog,
            private userservice: UserService,
            private router: Router) {
    this.getData();
  }

  public selectRow(row) {
    const id = row.id;
    console.log(row.id);
  }

  public getData() {
    this.userservice.getAllUser().subscribe(
      data => {
        const json = JSON.parse(JSON.stringify(data));
        if (json.code === 0) {
          const arr = json.accounts;
          const mapedArr = arr.map(x => {
            x.roles = x.roles.map(r => {
              return r.name;
            });
            x.fullname = x.firstname + ' ' + x.lastname;
            return x;
          });
          this.ELEMENT_DATA = mapedArr;
          this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.selection = new SelectionModel<Users>(true, []);
        } else {
          console.log('It\'s not a response : ' + JSON.stringify(data));
        }

      }
    );
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  isActive(str: string): boolean {
    return str === 'Y' ;
  }

  openAddUserDialog() {
    const dialogRef = this.dialog.open(AddUserDialogComponent, {
      width: '250px',
      data: {name: 'this.name', animal: 'this.animal'}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('add user dialog closed');
      this.getData();
    });
  }

  deleteUser() {
    const dialogRef = this.dialog.open(DeleteUserDialogComponent, {
      width: '250px',
      data: this.selection.selected
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('delete user dialog closed');
      this.selection.clear();
      this.getData();
    });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  isSelectedAtleaseOne() {
    return this.selection.selected.length > 0;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

}

export interface Users {
  id: number;
  username: String;
  firstname: string;
  lastname: String;
  roles: string;
  reserved: string;
  active: string;
  status: string;
}

export class UsersDataSource extends DataSource<Users> {
  constructor(private data: Users[]) {
    super();
  }
   /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Users[]> {
    return observableOf(this.data);
  }

  disconnect() {}

}

@Component({
  selector: 'app-add-user-dialog',
  templateUrl: 'add-user-dialog.component.html'
})
export class AddUserDialogComponent implements OnInit {

  constructor(
    private userservice: UserService,
    private dialogRef: MatDialogRef<AddUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private formBuilder: FormBuilder) { }

  username: string;
  password: string;
  firstname: string;
  lastname: string;
  form: FormGroup;
  roles: [any];
  currentRoleId: number;

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
      firstname: [null, [Validators.required]],
      lastname: [null, [Validators.required]]
    });

    this.userservice.getAllRolesUnderMe().subscribe(
      data => {
        const response = JSON.parse(JSON.stringify(data));
        if (response.code === 0) {
          this.roles = response.roles;
        } else {
          alert(response.message);
          this.dialogRef.close();
        }
        if (this.roles.length > 0) {
          console.log('this.roles[0].roleId ' + this.roles[0].roleId);
          this.currentRoleId = this.roles[0].roleId;
        }
      }
    );
  }

  onCancelClick() {
    this.dialogRef.close();
  }

  onCreateClick() {
    this.userservice.createUser(this.username, this.password, this.firstname, this.lastname, this.currentRoleId).pipe(
      finalize(() => this.dialogRef.close()),
    ).subscribe(
      data => {
        console.log(data);
        if (JSON.parse(JSON.stringify(data)).code === 0) {
          console.log('create user ' + this.username + ' successfully.');
        } else {
          console.log('unknown reponse : ' + data);
        }
      }
    );
  }
}
