import { Component, OnInit, Input } from '@angular/core';
import { PackageFragment } from '../../../../_models/package';

@Component({
  selector: 'app-package-fragment',
  templateUrl: './package-fragment.component.html',
  styleUrls: ['./package-fragment.component.css']
})
export class PackageFragmentComponent implements OnInit {

  @Input() fragment: PackageFragment;

  constructor() { }

  ngOnInit() {
  }

}
