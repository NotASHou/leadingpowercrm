import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackageFragmentComponent } from './package-fragment.component';

describe('PackageFragmentComponent', () => {
  let component: PackageFragmentComponent;
  let fixture: ComponentFixture<PackageFragmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackageFragmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackageFragmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
