import { Package, PackageFragmentList, PackageType } from './../../../_models/package';
import { switchMap } from 'rxjs/operators';
import { ParamMap, ActivatedRoute, Router } from '@angular/router';
import { UserService } from './../../../_services/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-package-detail',
  templateUrl: './package-detail.component.html',
  styleUrls: ['./package-detail.component.css']
})
export class PackageDetailComponent implements OnInit {

  type: PackageType;
  package: Package;
  packageFList: PackageFragmentList;

  constructor(private user: UserService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = +params['id']; // (+) converts string 'id' to a number
      this.type = params['type'];

      this.user.packageById(this.type, id).subscribe(data => {
        const json = JSON.parse(JSON.stringify(data));
        if (json.code === 0) {
          const p: Package = new Package(json.lprpackage);
          this.packageFList = p.fragmentList();
          console.log(this.packageFList);
          this.package = p;
        } else if (json.message) {
          alert(json.message);
        } else {
          console.log('It\'s not a response : ' + JSON.stringify(data));
        }
      });
    });
  }

  onAdd() {
    this.packageFList.addLast();
  }

  onSave() {
    const data = this.packageFList.toData();
    this.user.updatePackage(this.type, this.package.id, this.package.name, data).subscribe(data => {
      const json = JSON.parse(JSON.stringify(data));
      if (json.code === 0) {
        this.onBack();
      } else if (json.message) {
        alert(json.message);
      } else {
        console.log('It\'s not a response : ' + JSON.stringify(data));
      }
    });

  }

  onBack() {
    this.router.navigate(['/crm/package/', this.type]);
  }
}
