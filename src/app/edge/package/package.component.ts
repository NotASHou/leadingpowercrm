import { ActivatedRoute } from '@angular/router';
import { Package, PackageType } from './../../_models/package';
import { UserService } from './../../_services/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.css']
})
export class PackageComponent implements OnInit {

  type: PackageType;
  flatPackageList: [Package];

  constructor(private user: UserService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.type = params['type'];
      this.load();
    });
  }

  add() {
    this.user.createPackage(this.type, 'NewPackage', '0,0').subscribe(data => {
      this.load();
    });
  }

  load() {
    this.user.packageList(this.type).subscribe(data => {
      const json = JSON.parse(JSON.stringify(data));
      if (json.code === 0) {
        this.flatPackageList = json.list;
      } else if (json.message) {
        alert(json.message);
      } else {
        console.log('It\'s not a response : ' + JSON.stringify(data));
      }
    });
  }
}
