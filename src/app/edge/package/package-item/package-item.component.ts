import { Package } from './../../../_models/package';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-package-item',
  templateUrl: './package-item.component.html',
  styleUrls: ['./package-item.component.css']
})
export class PackageItemComponent implements OnInit {

  @Input() package: Package;
  constructor() { }

  ngOnInit() {
  }

}
