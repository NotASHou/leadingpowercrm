export class GeneralResponse {
  code: number;
  message: String;

  constructor(data: any) {
    // const json = JSON.parse(JSON.stringify(data));
    this.code = data.code;
    this.message = data.message;
  }
}
