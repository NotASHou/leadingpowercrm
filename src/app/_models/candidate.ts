export enum CandidateStatus {
  idle = 'idle',
  pass = 'pass',
  clientinterview = 'clientinterview',
  firstday = 'firstday',
  trainingcomplete = 'trainingcomplete',
  garanteeneed = 'garanteeneed',
  reject = 'reject',
  noshow = 'noshow',
  dropout = 'dropout',
  terminate = 'terminate'
}

export interface CandidateTableField {
  id: number;
  name: string;
  surname: string;
  status: CandidateStatus;
  mobile: string;
  registerDate: Date;
  rawData: any;
}

export class CandidateDetailData {
  name: string;
  surname: string;
  nickname: string;
  mobile: string;

  nationalityId: number;
  dateOfBirth: Date;
}

export class CandidateStatusData {
  status: CandidateStatus;
  salary: number;
  note: string;
  clientid: number;
  branchid: number;
  projectid: number;
  positionid: number;
  date: Date;
}

export class Nationality {
  id: number;
  name: string;

  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
  }
}

export class SplittedDate {
  date: Date;
  time: string;

  constructor(date: Date, time: string) {
    this.date = date;
    this.time = time;
  }

  mergeDate(): Date {
    if (this.date && this.time) {
      const date = new Date(this.date);
      date.setHours(+this.time.split(':')[0]);
      date.setMinutes(+this.time.split(':')[1]);

      return date;
    } else {
      return null;
    }
  }
}

export class ClientInterviewDate {
  date: Date;
  list: any[];
}
