import { element } from 'protractor';
export class Package {
  id: number;
  name: string;
  data: string;

  constructor(obj: any) {
    this.id = obj.id;
    this.name = obj.name;
    this.data = obj.data;
  }

  public fragmentList(): PackageFragmentList {
    let list: PackageFragment[];
    const arr = this.data.split(',');

    list = [];
    for (let i = 0 ; i < arr.length ; i += 2) {
      const f = new PackageFragment(+arr[i], +arr[i + 1]);
      if (list.length > 0) {
        list[list.length - 1].nextFragment = f;
      }
      list.push(f);
    }
    list[list.length - 1].nextFragment = new PackageFragment(null, null);

    const l = new PackageFragmentList();
    l.list = list;
    return l;
  }
}

export class PackageFragmentList {
  list: PackageFragment[];

  addLast() {
    const last = new PackageFragment(this.list[this.list.length - 1].lowerBound, 0);
    last.nextFragment = new PackageFragment(null, null);
    this.list[this.list.length - 1].nextFragment = last;
    this.list.push(last);
  }

  toData(): string {
    let data = '';
    this.list.forEach(x => {
      if (data === '') {
        data = x.lowerBound + ',' + x.value;
      } else {
        data = data + ',' + x.lowerBound + ',' + x.value;
      }
    });
    console.log(data);
    return data;
  }
}

export class PackageFragment {
  lowerBound: number;
  value: number;
  nextFragment: PackageFragment;

  constructor(lowerBound: number, value: number) {
    this.lowerBound = lowerBound;
    this.value = value;
    this.nextFragment = null;
  }
}

export enum PackageType {
  flat = 'flat',
  percent = 'percent',
  contract = 'contract'
}
