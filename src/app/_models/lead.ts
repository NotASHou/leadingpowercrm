import { GeneralResponse } from './general';
export enum LeadStatus {
  noaction = 'noaction',
  call = 'call',
  conversation = 'conversation',
  interest = 'interest',
  appoint = 'appoint',
  pass = 'pass'
}

export interface LeadTableField {
  name: string;
  surname: string;
  nickname: string;
  mobile: string;
  sourcename: string;
  recruiter: string;
  rawData: any;
}

export class LeadData {
  name: string;
  surname: string;
  nickname: string;
  mobile: string;

  leadSourceId: number;
  status: LeadStatus;
  datetime: Date;
}

export class InterviewDate {
  date: Date;
  list: any[];
}

export class LeadByIdResponse extends GeneralResponse {
  lead: any;

  constructor(data: any) {
    super(data);
    this.lead = data.lead;
  }
}
