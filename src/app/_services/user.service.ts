
import {empty as observableEmpty,  Observable } from 'rxjs';

import {catchError, retry} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders, HttpResponse, HttpParams } from '@angular/common/http';

import { Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { Globals } from '../globals';
import { HttpErrorResponse } from '@angular/common/http/src/response';




import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { CandidateStatus } from '../_models/candidate';
import { PackageType } from '../_models/package';

@Injectable()
export class UserService {
  constructor(private globals: Globals,
    private authen: AuthenticationService,
    private http: HttpClient,
    private router: Router,
    private datePipe: DatePipe ) { }

  get(url: string) {
    const credentials = this.authen.readCredentials();
    if (!credentials) {
      this.router.navigate(['/login'], { queryParams: { returnUrl: this.router.url }});
    }
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': credentials.token_type + ' ' + credentials.access_token})
    };
    return this.handleError(this.http.get(url, httpOptions));
  }

  post(url: string, body: any) {
    const credentials = this.authen.readCredentials();
    if (!credentials) {
      this.router.navigate(['/login'], { queryParams: { returnUrl: this.router.url }});
    }
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': credentials.token_type + ' ' + credentials.access_token})
    };
    return this.handleError(this.http.post(url, body, httpOptions));
  }

  handleError(request: Observable<Object>) {
    return request.pipe(retry(3),catchError((httperrorres: HttpErrorResponse) => {
      const error = (<HttpErrorResponse>httperrorres).error;
      if (error instanceof Error) {
        console.error('An error occurred:', error.message);
      } else if (error.error) {
        if (error.error === 'invalid_token') {
          console.log(error.error);
          this.router.navigate(['/login'], { queryParams: { returnUrl: this.router.url }});
        } else if (error.error === 'unauthorized') {
          console.log(error.error);
        } else if (error.error === 'access_denied') {
          console.log(error.error);
        } else {
          console.error('Nott don\'t know how to handle it, but will learn now!! : ' + error.error);
        }
      } else {
        console.error('Nott don\'t know how to handle it, but will learn now!! : ' + JSON.stringify( error));
      }

      return observableEmpty();
    }),);
  }

  getAllUser() {
    const url = this.globals.listuser_url;
    return this.get(url);
  }

  createUser(user: String, pass: String, firstname: String, lastname: String, roleId: number) {
    const url = this.globals.adduser_url;
    const body = {'username' : user, 'password': pass, 'firstname': firstname, 'lastname': lastname, 'roleId': roleId};
    return this.post(url, body);
  }

  deleteUser(selection: Array<number>) {
    const url = this.globals.deleteuser_url;
    const body = {'ids' : JSON.parse(JSON.stringify(selection))};
    return this.post(url, body);
  }

  getAllRolesUnderMe() {
    const url = this.globals.listroleunderme_url;
    const body = {};
    return this.post(url, body);
  }

  myRole() {
    const url = this.globals.myrole_url;
    const body = {};
    return this.post(url, body);
  }

  leadList() {
    const url = this.globals.listlead_url;
    return this.get(url);
  }

  leadsourceList() {
    const url = this.globals.listleadsource_url;
    return this.get(url);
  }

  createLead(name: string, surname: string, nickname: string, mobile: string, sourceId: number) {
    const url = this.globals.addlead_url;
    const body = {
      'name': name,
      'surname': surname,
      'nickname': nickname,
      'mobile': mobile,
      'leadSourceId': sourceId
    };
    return this.post(url, body);
  }

  createLeadSource(name: string, cost: number) {
    const url = this.globals.addleadsource_url;
    const body = {
      'name': name,
      'cost': cost,
    };
    return this.post(url, body);
  }

  clientList() {
    const url = this.globals.listclient_url;
    return this.get(url);
  }

  branchList(clientId: number) {
    const url = this.globals.listbranch_url;
    const credentials = this.authen.readCredentials();
    if (!credentials) {
      this.router.navigate(['/login'], { queryParams: { returnUrl: this.router.url }});
    }
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': credentials.token_type + ' ' + credentials.access_token}),
      params: new HttpParams().set('clientId', '' + clientId)
    };

    return this.handleError(this.http.get(url, httpOptions));
  }

  projectList(branchId: number) {
    const url = this.globals.listproject_url;
    const credentials = this.authen.readCredentials();
    if (!credentials) {
      this.router.navigate(['/login'], { queryParams: { returnUrl: this.router.url }});
    }
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': credentials.token_type + ' ' + credentials.access_token}),
      params: new HttpParams().set('branchId', '' + branchId)
    };
    return this.handleError(this.http.get(url, httpOptions));
  }

  positionList(projectId: number) {
    const url = this.globals.listposition_url;
    const credentials = this.authen.readCredentials();
    if (!credentials) {
      this.router.navigate(['/login'], { queryParams: { returnUrl: this.router.url }});
    }
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': credentials.token_type + ' ' + credentials.access_token}),
      params: new HttpParams().set('projectId', '' + projectId)
    };
    return this.handleError(this.http.get(url, httpOptions));
  }

  createClient(name: string) {
    const url = this.globals.addclient_url;
    const body = {
      'name': name,
    };
    return this.post(url, body);
  }

  createBranch(name: string, clientId: number) {
    const url = this.globals.addbranch_url;
    const body = {
      'name': name,
      'clientId': clientId
    };
    return this.post(url, body);
  }

  createProject(name: string, branchId: number) {
    const url = this.globals.addproject_url;
    const body = {
      'name': name,
      'branchId': branchId
    };
    return this.post(url, body);
  }

  createPosition(name: string, projectId: number) {
    const url = this.globals.addposition_url;
    const body = {
      'name': name,
      'projectId': projectId
    };
    return this.post(url, body);
  }

  deleteClient(clientId: number) {
    const url = this.globals.deleteclient_url;
    const body = clientId;
    return this.post(url, body);
  }

  deleteBranch(branchId: number) {
    const url = this.globals.deletebranch_url;
    const body = branchId;
    return this.post(url, body);
  }

  deleteProject(projectId: number) {
    const url = this.globals.deleteproject_url;
    const body = projectId;
    return this.post(url, body);
  }

  deletePosition(positionId: number) {
    const url = this.globals.deleteposition_url;
    const body = positionId;
    return this.post(url, body);
  }

  updateClient(id: number, name: string) {
    const url = this.globals.updateclient_url;
    const body = {
      'id': id,
      'name': name,
    };
    return this.post(url, body);
  }

  updateBranch(id: number, name: string) {
    const url = this.globals.updatebranch_url;
    const body = {
      'id': id,
      'name': name,
    };
    return this.post(url, body);
  }

  updateProject(id: number, name: string) {
    const url = this.globals.updateproject_url;
    const body = {
      'id': id,
      'name': name,
    };
    return this.post(url, body);
  }

  updatePosition(id: number, name: string) {
    const url = this.globals.updateposition_url;
    const body = {
      'id': id,
      'name': name,
    };
    return this.post(url, body);
  }

  deleteLead(selection: Array<number>) {
    const url = this.globals.deletelead_url;
    const body = {'ids' : JSON.parse(JSON.stringify(selection))};
    return this.post(url, body);
  }

  editLead(id: number, name: string, surname: string, nickname: string, mobile: string,
    currentSourceId: number, currentStatus: string, datetime: Date) {
    const url = this.globals.updatelead_url;
    const body = {
      'id': id,
      'name' : name,
      'surname': surname,
      'nickname': nickname,
      'mobile': mobile,
      'currentSourceId': currentSourceId,
      'currentStatus': currentStatus,
      'datetime': datetime};
    return this.post(url, body);
  }

  applicationList() {
    const url = this.globals.listapplication_url;
    return this.get(url);
  }

  leadPhone(leadId: number) {
    const url = this.globals.leadphone_url;
    const body = {
      'leadId': leadId,
    };
    return this.post(url, body);
  }

  candidateById(id: number) {
    const url = this.globals.candidatebyid_url;
    const body = {
      'id': id
    };
    return this.post(url, body);
  }

  saveCandidateStatus(candidateId: number, status: CandidateStatus,
                      datetime: Date,
                      clientId: number, branchId: number, projectId: number, positionId: number, note: string, salary: number) {
    const url = this.globals.savecandidatestatus_url;
    const body = {
      'candidateId': candidateId,
      'status': status,
      'date': datetime,
      'clientId': clientId,
      'branchId': branchId,
      'projectId': projectId,
      'positionId': positionId,
      'note': note,
      'salary': salary
    };
    return this.post(url, body);
  }

  saveCandidateDetail(candidateId: number,
    name: string,
    surname: string,
    nickname: string,
    mobile: string,
    nationalityId: number,
    dateOfBirth: Date) {

    const url = this.globals.savecandidatedetail_url;
    const body = {
      'candidateId': candidateId,
      'name': name,
      'surname': surname,
      'nickname': nickname,
      'mobile': mobile,
      'dateOfBirth': dateOfBirth,
      'nationalityId': nationalityId
    };
    return this.post(url, body);
  }

  nationalityList() {
    const url = this.globals.listnationality_url;
    return this.get(url);
  }

  myInterview() {
    const url = this.globals.myInterview_url;
    const body = {};
    return this.post(url, body);
  }

  clientInterview() {
    const url = this.globals.clientInterview_url;
    const body = {};
    return this.post(url, body);
  }

  incomplete() {
    const url = this.globals.incomplete_url;
    const body = {};
    return this.post(url, body);
  }

  packageList(type: PackageType) {
    let url: string;
    switch (type) {
      case PackageType.flat :
      url = this.globals.listflatpackage_url;
      break;
      case PackageType.percent :
      url = this.globals.listpercentpackage_url;
      break;
      case PackageType.contract :
      url = this.globals.listcontractpackage_url;
      break;
    }

    const body = {};
    return this.post(url, body);
  }

  createPackage(type: PackageType, name: string, data: string) {
    let url: string;
    switch (type) {
      case PackageType.flat :
      url = this.globals.addflatpackage_url;
      break;
      case PackageType.percent :
      url = this.globals.addpercentpackage_url;
      break;
      case PackageType.contract :
      url = this.globals.addcontractpackage_url;
      break;
    }

    const body = {
      'name': name,
      'data': data
    };
    return this.post(url, body);
  }

  updatePackage(type: PackageType, id: number, name: string, data: string) {
    let url: string;
    switch (type) {
      case PackageType.flat :
      url = this.globals.updateflatpackage_url;
      break;
      case PackageType.percent :
      url = this.globals.updatepercentpackage_url;
      break;
      case PackageType.contract :
      url = this.globals.updatecontractpackage_url;
      break;
    }

    const body = {
      'id': id,
      'name': name,
      'data': data
    };
    return this.post(url, body);
  }

  packageById(type: PackageType, id: number) {
    let url: string;
    switch (type) {
      case PackageType.flat :
      url = this.globals.flatpackagebyid_url;
      break;
      case PackageType.percent :
      url = this.globals.percentpackagebyid_url;
      break;
      case PackageType.contract :
      url = this.globals.contractpackagebyid_url;
      break;
    }

    const body = {
      'id': id
    };
    return this.post(url, body);
  }

  leadById(id: number) {
    const url = this.globals.leadbyid_url;
    const body = {
      'id': id
    };
    return this.post(url, body);
  }
}
