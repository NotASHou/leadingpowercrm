import { CandidateStatusComponent } from './../edge/application/edit-application/candidate-status/candidate-status.component';
import { CandidateTableField, CandidateDetailData, SplittedDate, CandidateStatusData } from './../_models/candidate';
import { Injectable } from '@angular/core';
import { CandidateDetailComponent } from '../edge/application/edit-application/candidate-detail/candidate-detail.component';
import { DatePipe } from '@angular/common';

@Injectable()
export class CandidateService {

  constructor() { }

  candidateObject2TableField(x: any): CandidateTableField {
    const y: CandidateTableField = {
      id: x.id,
      name: x.lead.name,
      surname: x.lead.surname,
      status: x.status,
      mobile: x.lead.mobile,
      registerDate: x.registerDate,
      rawData: x
    };
    return y;
  }

  candidateDetailDataFromForm(component: CandidateDetailComponent): CandidateDetailData {
    const c = new CandidateDetailData();
    c.name = component.data.name;
    c.surname = component.data.surname;
    c.nickname = component.data.nickname;
    c.mobile = component.data.mobile;
    c.dateOfBirth = component.data.dateOfBirth;
    c.nationalityId = component.data.nationalityId;

    return c;
  }

  candidateStatusDataFromForm(component: CandidateStatusComponent): CandidateStatusData {
    const c = new CandidateStatusData();
    c.status = component.data.status;
    c.date = new SplittedDate(component.data.date, component.data.time).mergeDate();
    c.clientid = component.data.clientId;
    c.branchid = component.data.branchId;
    c.projectid = component.data.projectId;
    c.positionid = component.data.positionId;
    c.salary = component.data.salary;
    c.note = component.data.note;
    return c;
  }

  splitDate(datetime: Date): SplittedDate {

    const datePipe = new DatePipe('en-US');
    const time = datePipe.transform(datetime, 'HH:mm'); // this.date.getHours() + ':' + this.date.getMinutes();
    const date = new Date(datetime);
    date.setHours(0);
    date.setMinutes(0);

    const split = new SplittedDate(date, time);
    return split;
  }
}
