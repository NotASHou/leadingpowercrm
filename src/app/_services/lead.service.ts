import { LeadTableField, LeadData, LeadStatus } from './../_models/lead';
import { Injectable } from '@angular/core';

@Injectable()
export class LeadService {

  constructor() { }

  leadObject2TableField(x: any): LeadTableField {
    const y: LeadTableField = {
      name: x.name,
      surname: x.surname,
      nickname: x.nickname,
      mobile: x.mobile,
      sourcename: x.source.name,
      recruiter: x.addby.username,
      rawData: x
    };
    return y;
  }

}
