
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs';

import { Globals } from '../globals';
import { JwtHelper } from 'angular2-jwt';
@Injectable()
export class AuthenticationService {

  jwtHelper: JwtHelper = new JwtHelper();

    constructor(private globals: Globals, private http: Http) { }

    login(username: string, password: string) {
      const headers = new Headers({ 'Accept': 'application/json',
                                    'Content-Type': 'application/x-www-form-urlencoded',
                                    'Authorization': 'Basic Y2xpZW50OnNlY3JldA==' });
      const options = new RequestOptions({ headers: headers });

      const body = 'password=' + password +
      '&username=' + username +
      '&grant_type=password' +
      '&scope=read%20write' +
      '&client_secret=secret' +
      '&client_id=client';

      return this.http.post(this.globals.auth_url, body, options).pipe(
      map(response => {
        const json = response.json();

        if (json && json.access_token) {
          this.writeCredentials(json);
        }

        return json;
      }));
    }

    logout() {
        // remove user from local storage to log user out
        this.writeCredentials(null);
    }

    writeCredentials(credentials: LoginResponse) {
      localStorage.setItem('credentials', JSON.stringify(credentials));
    }

    readCredentials(): LoginResponse {
      return JSON.parse(localStorage.getItem('credentials'));
    }

    readUsername(): string {

      return this.jwtHelper.decodeToken(this.readCredentials().access_token).user_name;
    }
}

export interface LoginResponse {
  access_token: string;
  token_type: string;
  refresh_token: string;
  expires_in: number;
  scope: string;
  jti: string;
}
