import { CandidateService } from './_services/candidate.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './edge/header/header.component';
import { EdgeComponent } from './edge/edge.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { LeadComponent } from './edge/lead/lead.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material';
import { MatSortModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { LeadSourceComponent } from './edge/lead-source/lead-source.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { UsersComponent, AddUserDialogComponent } from './edge/users/users.component';
import { Globals } from './globals';
import { HttpModule, ConnectionBackend } from '@angular/http';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AlertComponent } from './_directives/index';
import { AuthGuard } from './_guards/index';
import { AlertService, AuthenticationService, UserService } from './_services/index';
import { RegisterComponent } from './register/register.component';
import { ClientComponent } from './edge/client/client.component';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { DeleteUserDialogComponent } from './edge/users/delete-user-dialog/delete-user-dialog.component';
import { AddLeadDialogComponent } from './edge/lead/add-lead-dialog/add-lead-dialog.component';
import { AddLeadSourceDialogComponent } from './edge/lead-source/add-lead-source-dialog/add-lead-source-dialog.component';
import { AddProjectDialogComponent } from './edge/client/add-project-dialog/add-project-dialog.component';
import { AddPositionDialogComponent } from './edge/client/add-position-dialog/add-position-dialog.component';
import { AddClientDialogComponent } from './edge/client/add-client-dialog/add-client-dialog.component';
import { AddBranchDialogComponent } from './edge/client/add-branch-dialog/add-branch-dialog.component';
import { DeleteDialogComponent } from './edge/client/delete-dialog/delete-dialog.component';
import { DeleteLeadDialogComponent } from './edge/lead/delete-lead-dialog/delete-lead-dialog.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { DatePipe } from '@angular/common';
import { ApplicationComponent } from './edge/application/application.component';
import { EditApplicationComponent } from './edge/application/edit-application/edit-application.component';
import { MatTabsModule } from '@angular/material/tabs';
import { CandidateStatusComponent } from './edge/application/edit-application/candidate-status/candidate-status.component';
import { CandidateDetailComponent } from './edge/application/edit-application/candidate-detail/candidate-detail.component';
import { LeadService } from './_services/lead.service';
import { MyInterviewComponent } from './edge/my-interview/my-interview.component';
import { InterviewDateComponent } from './edge/my-interview/interview-date/interview-date.component';
import { InterviewComponent } from './edge/my-interview/interview-date/interview/interview.component';
import { ClientInterviewComponent } from './edge/client-interview/client-interview.component';
import { ClientInterviewDateComponent } from './edge/client-interview/client-interview-date/client-interview-date.component';
import { CInterviewComponent } from './edge/client-interview/client-interview-date/c-interview/c-interview.component';
import { IncompleteComponent } from './edge/incomplete/incomplete.component';
import { PackageComponent } from './edge/package/package.component';
import { PackageItemComponent } from './edge/package/package-item/package-item.component';
import { PackageDetailComponent } from './edge/package/package-detail/package-detail.component';
import { PackageFragmentComponent } from './edge/package/package-detail/package-fragment/package-fragment.component';
import { MatRippleModule } from '@angular/material/core';
import { LeadDetailComponent } from './edge/lead/lead-detail/lead-detail.component';

@NgModule({
  entryComponents: [
    AddUserDialogComponent,
    DeleteUserDialogComponent,
    AddLeadDialogComponent,
    AddLeadSourceDialogComponent,
    AddClientDialogComponent,
    AddBranchDialogComponent,
    AddProjectDialogComponent,
    AddPositionDialogComponent,
    DeleteDialogComponent,
    DeleteLeadDialogComponent,
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    EdgeComponent,
    LeadComponent,
    LeadSourceComponent,
    UsersComponent,
    AlertComponent,
    RegisterComponent,
    ClientComponent,
    AddUserDialogComponent,
    DeleteUserDialogComponent,
    AddLeadDialogComponent,
    AddLeadSourceDialogComponent,
    AddProjectDialogComponent,
    AddPositionDialogComponent,
    AddClientDialogComponent,
    AddBranchDialogComponent,
    DeleteDialogComponent,
    DeleteLeadDialogComponent,
    ApplicationComponent,
    EditApplicationComponent,
    CandidateStatusComponent,
    CandidateDetailComponent,
    MyInterviewComponent,
    InterviewDateComponent,
    InterviewComponent,
    ClientInterviewComponent,
    ClientInterviewDateComponent,
    CInterviewComponent,
    IncompleteComponent,
    PackageComponent,
    PackageItemComponent,
    PackageDetailComponent,
    PackageFragmentComponent,
    LeadDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    HttpModule,
    MatSlideToggleModule,
    FormsModule,
    HttpClientModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTabsModule,
    MatRippleModule
  ],
  providers: [
    Globals,
    AuthGuard,
    AlertService,
    AuthenticationService,
    UserService,
    CandidateService,
    LeadService,
    DatePipe
    // provider used to create fake backend
    // fakeBackendProvider

  ],
  bootstrap: [
    AppComponent]
})
export class AppModule { }
