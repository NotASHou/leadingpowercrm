import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService, AuthenticationService } from '../_services/index';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: any = {};
  loading = false;
  returnUrl: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService) { }

  ngOnInit() {
    // reset login status
    this.authenticationService.logout();

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login() {
    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password).subscribe(
      res => {
        this.router.navigate([this.returnUrl]);
      },
      error => {
        console.error('error ' + error);
        this.alertService.error(error);
        this.loading = false;
      });
        // .subscribe(
        //     data => {
        //       this.router.navigate([this.returnUrl]);
        //     },
        //     err => {
        //       console.log('error ' + err);
        //       this.alertService.error(err);
        //       this.loading = false;
        //     });
  }
}
