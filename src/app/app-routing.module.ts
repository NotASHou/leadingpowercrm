import { LeadDetailComponent } from './edge/lead/lead-detail/lead-detail.component';
import { EditApplicationComponent } from './edge/application/edit-application/edit-application.component';
import { PackageDetailComponent } from './edge/package/package-detail/package-detail.component';
import { PackageComponent } from './edge/package/package.component';
import { IncompleteComponent } from './edge/incomplete/incomplete.component';
import { ClientInterviewComponent } from './edge/client-interview/client-interview.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { EdgeComponent } from './edge/edge.component';
import { LeadComponent } from './edge/lead/lead.component';
import { LeadSourceComponent } from './edge/lead-source/lead-source.component';
import { UsersComponent } from './edge/users/users.component';
import { RegisterComponent } from './register/register.component';
import { ClientComponent } from './edge/client/client.component';
import { AuthGuard } from './_guards/auth.guard';
import { ApplicationComponent } from './edge/application/application.component';
import { MyInterviewComponent } from './edge/my-interview/my-interview.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/crm', pathMatch: 'full' },
  { path: 'crm', component: EdgeComponent, canActivate: [AuthGuard], children: [
    { path: 'lead', component: LeadComponent },
    { path: 'lead/:id', component: LeadDetailComponent },
    { path: 'lead-source', component: LeadSourceComponent },
    { path: 'users', component: UsersComponent },
    { path: 'client', component: ClientComponent },
    { path: 'application', component: ApplicationComponent },
    { path: 'application/:id', component: EditApplicationComponent },
    { path: 'my-interview', component: MyInterviewComponent },
    { path: 'client-interview', component: ClientInterviewComponent },
    { path: 'incomplete', component: IncompleteComponent },
    { path: 'package/:type', component: PackageComponent },
    { path: 'package/:type/:id', component: PackageDetailComponent },
  ]},
  { path: 'login', component: LoginComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '/crm' }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
